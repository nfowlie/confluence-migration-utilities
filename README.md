# OVERVIEW #

Migrating between Confluence instances is often painful, time consuming and full of "gotchas". New user directories mean new user ids, 'cruft' from previously installed plugins can cause import failures or pollute your shiny new Confluence install with broken data, Hyperlinks between Confluence pages may break, and often it's not feasible to manually verify each page for migration errors.

This repo has a collection of scripts developed from personal experience to automate away some of the more tedious aspects of Confluence data migrations. Most of these scripts were built for specific Confluence versions and would require additional testing for anything lower than v5.3.1 or higher than v5.5.3.

# Features #
## Convert Tiny Urls ##
Tiny Urls are not updated during Confluence migrations. As a result they break during migrations, with no easy method of determining which page the url used to link to.

The Tiny Url Converter script works one of two ways:

1. The Smart Way
Point the script at an entities.xml file generated as part of a Confluence XML backup and it will perform the url conversions and spit out a new entities.xml ready for importing into another Confluence instance.

This is the recommended method, particularly for large, content heavy Spaces.

2. The Dumb Way
Provide the script with the url of your Confluence instance, a space key, and the credentials of a Confluence Administrator and it will crawl through each page, converting Tiny Urls as it goes. For this to work you will need to enable the Remote API (JSON-RPC). Converting the Urls in this manner is orders of magnitude slower than method #1, can put a strain on your Confluence instance and should be avoided where possible.

## Re-map User Accounts ##
Internally, Confluence associates a GUID to each user account on each user directory. As you move Space content to a new Confluence instance, or create & destroy user directories, the GUID associated with each user changes. The result is page/comment authors appearing a "Unknown User", ditto for the uploaders of attachments. Additionally users may disappear from watchers/page restrictions lists as Confluence would no longer be able to find a user account with the given GUID.

The User Mapper script provides a way to re-map the old user ids to their new ones prior to performing an XML import to help preserve this information and minimize the chance of watches/page restrictions lists being messed up.

## Identify Space dependencies ##
When migrating Confluence Spaces one-by-one, it's guaranteed some hyperlinks between pages will break, either due to the Space not existing on the new Confluence instance, or because the user has manually entered a hyperlink instead of using the "search" feature to link to a page.

The Content Analysis scripts will generate a space-level report, outlining which hyperlinks are likely to break during migrations, and the exact pages where the hyperlinks are used. This will make your Space Administrators much happier, instead of having to manually check every page within their space, you can give them a list of the exact hyperlinks and pages they need to check.

## Identify unsupported macros/user macros ##
Over time Confluence instances tend to build up alot of cruft as plugins are installed/uninstalled, user macros are created, renamed and eventually deleted etc. The end result is bunches of pages with "Unknown Macro" warnings, inaccessible content, ugly exceptions in the application logs and degredated Confluence performance.

To avoid import the cruft into your new Confluence instance, the Content Analysis scripts will also analyze the content of pages for any macros that have been deemed as unsupported, using a whitelist that you control. Over time the scripts may be updated to remove some of the unsupported macro data, but for now they generate a summary of the unsupported macros found, and the page they reside on. Content owners can then update the problematic pages, without having to manually check every page within their Space.

## Bulk group permissioning ##
The ever popular Confluence CLI plugin does a create job of automating tasks such as bulk editing group permissions, however sometimes restrictions mean you're unable to leverage the plugin, and sql scripts often aren't feasible as they would require an outage period. 

The Group Management script will provide you with a plugin-free way of bulk adding/removing users from Confluence groups (*Remote API must be enabled). It's designed in a way that it can be used as part of other automation scripts, cron jobs etc to help you perform periodic maintenance of group permissions (ie: cleaning up inactive user accounts, enforcing strict controls around membership on the Confluence Administrators group).

## Content auditing ##
Some organisations may require close management of Confluence content to minimize the risk of sensitive information being exposed to unauthorized individuals (passwords, financial information, client details etc).

The Content Auditing scripts provide a way to do a "best effort" audit of Confluence to flag problematic content (via keywords and regular expressions). Over time the scripts may be updated to automatically remove said content (including editing previous versions of pages). For now, they simply generate a report detailing the offending pages, and the reason for the page being flagged.

## Audit User Permission ##
Similar to content auditing, some organisations may have a need to closely manage the permissions given to Confluence Spaces to reduce the risk of unauthorized individuals accessing restricted information.

The User Auditing scripts will generate a report outlining offending Spaces where Space permissions are not inline with the Terms of Use policy. Again, in time they may be updated to automatically reset Space permissions to assist with automated maintenance/risk management activities.

## How do I get set up? ##

* Install Python 3.4 (https://www.python.org/downloads/)
* Look at the ____main____ method of the scripts to figure out how they work, or wait for me to finish updating the documentation ;)

## What now? Where's all the scripts? ##
Alot of the scripts mentioned above are currently being re-written so they are usable across different Confluence environments/configurations, and can be setup as part of automated tasks/cron jobs/scheduled windows tasks. Stay tuned for updates.