﻿"""
Unit tests for the progressbar module.
"""

import unittest

from io import StringIO
from unittest.mock import patch
from util import ProgressBar

class TestStartProgressMethod(unittest.TestCase):
    """
    Unit tests for the ProgressBar.start_progress(str) method.
    """

    __title__ = None
    __all_dashes__ = None
    __all_backspaces__ = None

    @classmethod
    def setUpClass(cls):
        cls.__title__ = "Aenean condimentum, metus id tristique eleifend"
        cls.__all_dashes__ = "-" * 40
        cls.__all_backspaces__ = "\b" * 41


    @patch("sys.stdout", new_callable=StringIO)
    def test_no_title_argument(self, mock_stdout):
        # pylint: disable=C0111

        expected_output = " : [{0}]{1}".format(
            self.__all_dashes__,
            self.__all_backspaces__
        )

        progress_bar = ProgressBar()
        progress_bar.start_progress(None)

        self.assertEqual(mock_stdout.getvalue(), expected_output)


    @patch("sys.stdout", new_callable=StringIO)
    def test_with_title_argument(self, mock_stdout):
        # pylint: disable=C0111

        expected_output = "{0} : [{1}]{2}".format(
            self.__title__,
            self.__all_dashes__,
            self.__all_backspaces__
        )

        progress_bar = ProgressBar()
        progress_bar.start_progress(self.__title__)

        self.assertEqual(mock_stdout.getvalue(), expected_output)


class TestUpdateProgressMethod(unittest.TestCase):
    """
    Unit tests for the ProgressBar.update_progress(float) method.
    """

    __title__ = None
    __all_dashes__ = None
    __all_hashes__ = None
    __all_backspaces__ = None

    __mock_stdout__ = None

    @classmethod
    def setUpClass(cls):
        cls.__title__ = "Aenean condimentum, metus id tristique eleifend"
        cls.__all_dashes__ = "-" * 40
        cls.__all_hashes__ = "#" * 40
        cls.__all_backspaces__ = "\b" * 41

    @patch("sys.stdout", new_callable=StringIO)
    def test_0_percent_argument(self, mock_stdout):
        # pylint: disable=C0111

        progress_bar = ProgressBar()
        progress_bar.update_progress(0)

        self.assertEqual(mock_stdout.getvalue(), "")


    @patch("sys.stdout", new_callable=StringIO)
    def test_50_percent_argument(self, mock_stdout):
        # pylint: disable=C0111

        progress_bar = ProgressBar()
        progress_bar.update_progress(50)

        self.assertEqual(mock_stdout.getvalue(), "#" * 20)


    @patch("sys.stdout", new_callable=StringIO)
    def test_100_percent_argument(self, mock_stdout):
        # pylint: disable=C0111

        progress_bar = ProgressBar()
        progress_bar.update_progress(100)

        self.assertEqual(mock_stdout.getvalue(), self.__all_hashes__)


class TestEndProgressMethod(unittest.TestCase):
    """
    Unit tests for the ProgressBar.end_progress() method.
    """

    __title__ = None
    __all_hashes__ = None


    @classmethod
    def setUpClass(cls):
        cls.__all_hashes__ = "#" * 40


    @patch("sys.stdout", new_callable=StringIO)
    def test_shows_100_percentage_complete(self, mock_stdout):
        # pylint: disable=C0111,C0103

        progress_bar = ProgressBar()
        progress_bar.end_progress()

        self.assertEqual(mock_stdout.getvalue(), "{0}]\n".format(self.__all_hashes__))
