﻿"""
Unit tests for the zipmanager module.
"""

import os.path
import shutil
import tempfile
import unittest
import zipfile

from util import Environment
from util import zipmanager

class TestExtractExportMethod(unittest.TestCase):
    """
    Unit tests for the zipmanager.extract_export(str) method.
    """

    __archive_directory__ = None
    __archive_file__ = None
    __invalid_zip__ = None

    @classmethod
    def setUpClass(cls):
        # Create a "fake" directory that looks like an extracted Confluence XML export.
        cls.__archive_directory__ = tempfile.mkdtemp()

        entities_file = os.path.join(cls.__archive_directory__, "entities.xml")
        descriptor_file = os.path.join(cls.__archive_directory__, "exportDescriptor.properties")

        attachment_folder = os.path.join(
            cls.__archive_directory__,
            "attachments",
            "12345678",
            "523534"
        )

        attachment_file = os.path.join(attachment_folder, "1")

        open(file=entities_file, mode="w+", encoding="UTF-8").close()
        open(file=descriptor_file, mode="w+", encoding="UTF-8").close()

        os.makedirs(name=attachment_folder)
        open(file=attachment_file, mode="w+", encoding="UTF-8").close()

        # Create a "fake" Confluence XML Export file.
        random_filename = None

        with tempfile.TemporaryFile() as temp_file:
            random_filename = temp_file.name

        cls.__archive_file__ = shutil.make_archive(
            base_name=random_filename,
            root_dir=cls.__archive_directory__,
            format="zip"
        )

        # Temporary file used to mimic a 'broken' zip file.
        with tempfile.NamedTemporaryFile(delete=False) as tmp_file:
            cls.__invalid_zip__ = tmp_file.name


    @classmethod
    def tearDownClass(cls):
        if os.path.exists(cls.__archive_directory__):
            shutil.rmtree(cls.__archive_directory__)

        if os.path.isfile(cls.__archive_file__):
            os.remove(cls.__archive_file__)

        if os.path.isfile(path=cls.__invalid_zip__):
            os.remove(cls.__invalid_zip__)


    def test_no_filename_argument(self):
        # pylint: disable=C0111

        with self.assertRaises(ValueError):
            zipmanager.extract_export(None)


    def test_not_a_zip_file(self):
        # pylint: disable=C0111

        with self.assertRaises(ValueError):
            zipmanager.extract_export(self.__invalid_zip__)


    def test_extract_zip_file(self):
        # pylint: disable=C0111

        extracted_directory = zipmanager.extract_export(self.__archive_file__)

        expected_entities_path = os.path.join(extracted_directory, "entities.xml")
        expected_descriptor_path = os.path.join(extracted_directory, "exportDescriptor.properties")

        expected_attachment_path = os.path.join(
            extracted_directory,
            "attachments",
            "12345678",
            "523534",
            "1"
        )

        self.assertIsNotNone(extracted_directory)
        self.assertTrue(os.path.isdir(extracted_directory))
        self.assertTrue(os.path.isfile(expected_entities_path))
        self.assertTrue(os.path.isfile(expected_descriptor_path))
        self.assertTrue(os.path.isfile(expected_attachment_path))

        shutil.rmtree(extracted_directory)


class TestCreateExportMethod(unittest.TestCase):
    """
    Unit tests for the zipmanager.create_export(str, str, Environment, str)
    method.
    """

    __space_key__ = "ABC"
    __environment__ = Environment.prod
    __source_directory__ = None
    __destination_archive__ = None

    @classmethod
    def setUpClass(cls):
        # Create a "fake" directory that looks like an extracted Confluence XML export.
        cls.__source_directory__ = tempfile.mkdtemp()

        entities_file = os.path.join(cls.__source_directory__, "entities.xml")
        descriptor_file = os.path.join(cls.__source_directory__, "exportDescriptor.properties")

        attachment_folder = os.path.join(
            cls.__source_directory__,
            "attachments",
            "12345678",
            "523534"
        )

        attachment_file = os.path.join(attachment_folder, "1")

        open(file=entities_file, mode="w+", encoding="UTF-8").close()
        open(file=descriptor_file, mode="w+", encoding="UTF-8").close()

        os.makedirs(attachment_folder)
        open(file=attachment_file, mode="w+", encoding="UTF-8").close()

        # Generate a random filename for the generated zip file.
        with tempfile.TemporaryFile() as temp_file:
            cls.__destination_archive__ = temp_file.name + ".zip"
            os.remove(temp_file.name)


    @classmethod
    def tearDownClass(cls):
        if os.path.exists(cls.__source_directory__):
            shutil.rmtree(cls.__source_directory__)


    def test_invalid_source_argument(self):
        # pylint: disable=C0111

        non_existant_file = None

        with tempfile.TemporaryFile() as temp_file:
            non_existant_file = temp_file.name + ".zip"
            os.remove(temp_file.name)

        with self.assertRaises(ValueError):
            zipmanager.create_export(
                non_existant_file,
                self.__destination_archive__,
                self.__environment__,
                self.__space_key__
            )


    def test_no_source_argument(self):
        # pylint: disable=C0111

        with self.assertRaises(ValueError):
            zipmanager.create_export(
                None,
                self.__destination_archive__,
                self.__environment__,
                self.__space_key__
            )


    def test_no_destination_argument(self):
        # pylint: disable=C0111

        with self.assertRaises(ValueError):
            zipmanager.create_export(
                self.__source_directory__,
                None,
                self.__environment__,
                self.__space_key__
            )


    def test_no_environment_argument(self):
        # pylint: disable=C0111

        with self.assertRaises(ValueError):
            zipmanager.create_export(
                self.__source_directory__,
                self.__destination_archive__,
                None,
                self.__space_key__
            )


    def test_no_space_key_argument(self):
        # pylint: disable=C0111

        with self.assertRaises(ValueError):
            zipmanager.create_export(
                self.__source_directory__,
                self.__destination_archive__,
                self.__environment__,
                None
            )


    def test_create_archive(self):
        # pylint: disable=C0111

        archive_file_path = zipmanager.create_export(
            self.__source_directory__,
            self.__destination_archive__,
            self.__environment__,
            self.__space_key__
        )

        temporary_directory = tempfile.mkdtemp()

        shutil.unpack_archive(archive_file_path, temporary_directory, "zip")

        expected_entities_file = os.path.join(temporary_directory, "entities.xml")
        expected_descriptor_file = os.path.join(temporary_directory, "exportDescriptor.properties")

        expected_attachment_folder = os.path.join(
            temporary_directory,
            "attachments",
            "12345678",
            "523534"
        )

        expected_attachment_file = os.path.join(expected_attachment_folder, "1")

        self.assertIsNotNone(
            archive_file_path,
            "{0} is not a file.".format(archive_file_path)
        )

        self.assertTrue(
            os.path.isfile(expected_entities_file),
            "{0} is not a file.".format(expected_entities_file)
        )

        self.assertTrue(
            os.path.isfile(expected_descriptor_file),
            "{0} is not a file.".format(expected_descriptor_file)
        )

        self.assertTrue(
            os.path.isdir(expected_attachment_folder),
            "{0} is not a directory.".format(expected_attachment_folder)
        )

        self.assertTrue(
            os.path.isfile(expected_attachment_file),
            "{0} is not a file.".format(expected_attachment_file)
        )

        self.assertTrue(
            zipfile.is_zipfile(archive_file_path),
            "{0} is not a valid zip file.".format(archive_file_path)
        )

        os.remove(archive_file_path)
        shutil.rmtree(temporary_directory)


if __name__ == "__main__":
    unittest.main()
