﻿"""
Unit tests for the content.tinyurl_converter module.
"""

import unittest

from content import tinyurl_converter

class TestTinyUrlToIdMethod(unittest.TestCase):
    """
    Unit tests for the content.tinyurl_converter.tinyurl_to_id(str, str)
    method.
    """

    __base_url__ = None
    __identifier__ = None
    __page_id__ = None

    @classmethod
    def setUpClass(cls):
        cls.__base_url__ = "localhost:8090"
        cls.__identifier__ = "B4AI"
        cls.__page_id__ = 557063


    def test_no_base_url_argument(self):
        # pylint: disable=C0111

        with self.assertRaises(ValueError):
            tinyurl_converter.tinyurl_to_id(
                None,
                self.__identifier__
            )


    def test_no_identifier_argument(self):
        # pylint: disable=C0111

        with self.assertRaises(ValueError):
            tinyurl_converter.tinyurl_to_id(
                self.__base_url__,
                None
            )


    def test_bad_identifier_argument(self):
        # pylint: disable=C0111

        with self.assertRaises(ValueError):
            tinyurl_converter.tinyurl_to_id(
                self.__base_url__,
                "BjdK305Ld;"
            )


    def test_converts_identifier(self):
        # pylint: disable=C0111

        expected_url = "{0}/pages/viewpage.action?pageId={1}".format(
            self.__base_url__,
            self.__page_id__
        )

        actual_url = tinyurl_converter.tinyurl_to_id(
            self.__base_url__,
            self.__identifier__
        )

        self.assertEqual(actual_url, expected_url)


    def test_formats_base_url_correctly(self):
        # pylint: disable=C0111

        base_url = "localhost/foo"

        expected_url = "{0}/pages/viewpage.action?pageId={1}".format(
            base_url,
            self.__page_id__
        )

        actual_url = tinyurl_converter.tinyurl_to_id(
            base_url,
            self.__identifier__
        )

        self.assertEqual(actual_url, expected_url)


class TestIdToTinyUrlMethod(unittest.TestCase):
    """
    Unit tests for the content.tinyurl_converter.id_to_tinyurl(int, str)
    method.
    """

    __base_url__ = None
    __identifier__ = None
    __page_id__ = None

    @classmethod
    def setUpClass(cls):
        cls.__base_url__ = "127.0.0.1"
        cls.__identifier__ = "CoAI"
        cls.__page_id__ = 557066


    def test_no_base_url_argument(self):
        # pylint: disable=C0111

        with self.assertRaises(ValueError):
            tinyurl_converter.id_to_tinyurl(
                None,
                self.__page_id__
            )


    def test_no_page_id_argument(self):
        # pylint: disable=C0111

        with self.assertRaises(ValueError):
            tinyurl_converter.id_to_tinyurl(
                self.__base_url__,
                None
            )


    def test_invalid_page_id_argument(self):
        # pylint: disable=C0111

        with self.assertRaises(ValueError):
            tinyurl_converter.id_to_tinyurl(
                self.__base_url__,
                "12345"
            )


    def test_converts_page_id_correctly(self):
        # pylint: disable=C0111

        expected_url = "{0}/x/{1}".format(self.__base_url__, self.__identifier__)

        actual_url = tinyurl_converter.id_to_tinyurl(
            self.__base_url__,
            self.__page_id__
        )

        self.assertEqual(actual_url, expected_url)


    def test_formats_base_url_correctly(self):
        # pylint: disable=C0111

        expected_url = "localhost:10000/confluence/x/{0}".format(self.__identifier__)

        actual_url = tinyurl_converter.id_to_tinyurl(
            "localhost:10000/confluence",
            self.__page_id__
        )

        self.assertEqual(actual_url, expected_url)


if __name__ == '__main__':
    unittest.main()
