﻿"""
Utility modules and classes used by the Confluence migration scripts to
process data in various ways.
"""
import sys
from enum import Enum, unique

@unique
class Environment(Enum):
    """
    Defines the type of environment that migration operations are being carried out on.
    """
    test = 1
    uat = 2
    prod = 3


class ProgressBar:
    """
    Displays an ASCII progress bar. Some migration scripts take a long time to
    process the Confluence data, so a visual indicator is needed so users can
    get a rough idea of how much longer the processing will take.
    """

    __progress__ = 0

    __complete_char__ = "#"
    __incomplete_char__ = "-"


    def start_progress(self, title: str):
        """
        Outputs an ASCII progress bar.

        Args:
            title (str): Label to display in front of the progress bar.
        """

        backspaces = "\b" * 41
        dashes = self.__incomplete_char__ * 40

        sys.stdout.write("{0} : [{1}]{2}".format(str(title or ""), str(dashes or ""), backspaces))
        sys.stdout.flush()
        self.__progress__ = 0


    def update_progress(self, percent: float):
        """
        Updates the progress bar.

        Args:
            percent (float): The percentage of the operation that is complete.

        Raises:
            ValueError: If `percent` is less than 0.
        """

        if percent < 0:
            raise ValueError("percent cannot be a negative value.")

        percent = int(percent * 40 // 100)

        hash_count = percent - self.__progress__

        sys.stdout.write(self.__complete_char__ * hash_count)
        sys.stdout.flush()

        self.__progress__ = percent


    def end_progress(self):
        """
        Outputs an ASCII progress bar indicating the operation is complete.
        """

        hash_count = 40 - self.__progress__

        sys.stdout.write("{0}]\n".format(self.__complete_char__ * hash_count))
        sys.stdout.flush()
