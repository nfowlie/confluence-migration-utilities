"""
This module provides methods to take a Confluence page id and convert it to
it's Tiny Url representation, and vice versa.
"""

import argparse
import base64

__original_url_format__ = "{0}/pages/viewpage.action?pageId={01}"
__encoded_url_format__ = "{0}/x/{1}"


def id_to_tinyurl(base_url: str, page_id: int):
    """
    Converts a Confluence page id into a Tiny Url.

    Args:
        base_url (str): The base url as defined in Confluence.
        page_id (str): The page id to be converted into a Tiny Url identifier.

    Raises:
        ValueError: If `base_url` or `page_id` are None or not strings.

    Returns:
        A Tiny Url for the specified Confluence page. For example,
        http://localhost:8090/x/B4AI
    """

    if base_url is None or not isinstance(base_url, str):
        raise ValueError("{0} is not a valid base url.".format(str(base_url)))

    if page_id is None or not isinstance(page_id, int):
        raise ValueError("{0} is not a valid page id.".format(str(page_id)))

    url_as_bytes = page_id.to_bytes(8, byteorder="little")
    encoded_url = base64.b64encode(url_as_bytes)

    identifier = encoded_url.decode("utf-8").rstrip("A=")

    return __encoded_url_format__.format(base_url, str(identifier))


def tinyurl_to_id(base_url: str, identifier: str):
    """
    Converts a Tiny Url identifier back to it's original page id.

    Args:
        base_url (str): The base url as defined in Confluence.
        identifier (str): The Tiny Url identifier to be converted to a page id.

    Raises:
        ValueError: If `base_url` or `identifier` are None or not strings.

    Returns:
        The original url to the Confluence page. For example,
        http://localhost:8090/pages/viewpage.action?pageId=557063.
    """

    if base_url is None or not isinstance(base_url, str):
        raise ValueError("base_url must be provided.")

    if identifier is None or not isinstance(identifier, str):
        raise ValueError("identifier must be provided.")

    padded_identifier = __add_base64_padding__(identifier)
    padded_identifier_as_bytes = padded_identifier.encode("utf-8")

    decoded_identifier = base64.b64decode(padded_identifier_as_bytes)

    page_id = int.from_bytes(decoded_identifier, byteorder="little")

    return __original_url_format__.format(base_url, str(page_id))


def __add_base64_padding__(identifier: str):
    """
    Pads the the Tiny Url identifier to make it a valid base 64 string that can
    be decoded.

    Args:
        identifier (str): The Tiny Url identifier to be padded.

    Raises:
        ValueError: If `identifier` is None.

    Returns:
        A valid base 64 encoded string.
    """

    if identifier is None or not isinstance(identifier, str):
        raise ValueError("{0} is not a valid identifier.".format(str(identifier)))

    # Code is a direct port from com.atlassian.pages.TinyUrl class. Seems to
    # have some irrelevant/dead code but I've left the logic as is (maybe it
    # covers a rare edge case or is needed for backwards compatibility
    # reasons?).
    if identifier[-1:] == "/":
        identifier = identifier[:-1]

    padded_value = ""

    for i in range(0, 11):
        if i >= len(identifier):
            padded_value += "A"
            continue

        char = identifier[i]

        if char == "-":
            padded_value += "/"
        elif char == " ":
            padded_value += "+"
        else:
            padded_value += char

    padded_value += "=\n"

    return padded_value


def __process_parameters__():
    """
    Processes the command line arguments provided by the user, and sets some
    sane defaults.
    """

    parser = argparse.ArgumentParser(
        description="Converts a Tiny Url to its original Confluence url, and vice versa."
    )

    parser.add_argument(
        "-u",
        "--url",
        help="Confluence base url.",
        action="store",
        type="string",
        dest="base_url"
    )

    parser.add_argument(
        "-i",
        "--identifier",
        help="Tiny Url identifier to be decoded.",
        action="store",
        type="string",
        dest="identifier"
    )

    parser.add_argument(
        "-p",
        "--pageid",
        help="Page Id to be converted to a Tiny Url.",
        action="store",
        type="string",
        dest="page_id"
    )

    parser.add_argument(
        "-a",
        "--action",
        help="""Action to perform ('tinyurl' to convert a page id to a tiny
            url, or 'pageid' to decode a Tiny Url identifer back to its 
            original url."""
    )

    options = parser.parse_args()

    if options.base_url is None:
        raise ValueError("Base URL must be specified. Did you forget the -u parameter?")
    else:
        options.base_url = options.base_url.lower()

    if options.action is None:
        options.action = "pageid"

    if options.action.lower() == "tinyurl":
        if options.page_id is None:
            raise ValueError("Page Id must be specified. Did you forget the -o parameter?")
    else:
        if options.identifier is None:
            raise ValueError(
                """Tiny Url identifier must be specified. Did you forget the
                -i parameter?"""
            )

    options.action = options.action.lower()

    return options


def __main__():
    """
    Main entry point when the module is run from the command line.

    Args:
        -u (--url): Confluence base url.
        -i (--identifier): Tiny Url identifier to be converted.
        -p (--pageid): Page Id to be converted into a Tiny Url.
        -a (--action): Action to perform ('tinyurl' to convert a page id to a
            tiny url, or 'pageid' to decode a Tiny Url identifer back to its
            original url.
    """

    parameters = __process_parameters__()

    if parameters.action == "tinyurl":
        print(tinyurl_to_id(parameters.base_url, parameters.page_id))
    else:
        print(id_to_tinyurl(parameters.base_url, parameters.identifier))


if __name__ == "__main__":
    __main__()
    exit(0)
